#!/bin/bash
#SBATCH --cpus-per-task=4
#SBATCH --mem=16G        # To edit according the video
#SBATCH --time=16:00:00  # To edit according the video
#SBATCH --partition=normal
#SBATCH --chdir=WORKDIR



#############
## TO EDIT ##
#############
#SBATCH --job-name="ANALYSIS_NAME-videos-conversion-zebrazoom"
#SBATCH --output=outputs/ANALYSIS_NAME-videos-conversion-zebrazoom_%A_%a.txt
#SBATCH --error=outputs/ANALYSIS_NAME-videos-conversion-zebrazoom_%A_%a.txt
#SBATCH --array=1-NB_VIDEOS




# Just a function to get the process duration
function time_human_readable () {
    if [ -z ${1} ]; then
        return 1
    fi
    DURATION=${1}
    echo "$(($DURATION / 3600)) hours, $(($DURATION % 3600 / 60)) minutes and $(($DURATION % 60)) seconds elapsed."
}

echo -e "--- \e[1;34mModule loading\e[0m ---"
module load ZebraZoom/0.96



#############
## TO EDIT ##
#############
export VIDEO_CONVERSION_CMD_FILE_PATH=commands-files/videos-conversion-commands-ANALYSIS_NAME.txt


export VIDEO_CONVERSION_CMD=$(sed -n ${SLURM_ARRAY_TASK_ID}p ${VIDEO_CONVERSION_CMD_FILE_PATH})

if [ ! -z "$VIDEO_CONVERSION_CMD" ]; then

    echo -e "--- \e[1;34mStart video conversion\e[0m ---"

    echo -e "\e[33m$VIDEO_CONVERSION_CMD\e[0m"

    START=$(date +%s)
    eval $VIDEO_CONVERSION_CMD
    END=$(date +%s)
    time_human_readable $((${END} - ${START}))

    echo -e "--- \e[1;34mEnd video conversion\e[0m ---"

fi

exit 0
